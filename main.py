"""Macros de Franck Chambon, Guillaume Connan et autres participants du forum NSI
Merci à eux
Voir https://mooc-forums.inria.fr/moocnsi/t/mkdocs-une-solution-ideale/1758

"""
from mkdocs_macros import fix_url 
import os

def pyodide():
    #macro de Guillaume Connan
    s =" <div> Code Python:</div><textarea id='code' style='width: 100%;'rows='10' ></textarea><button onclick='evaluatePython()' class='execution'>Exécuter</button><br><div>Résultat:</div><textarea id='output' style='width: 100%;' rows='10' disabled></textarea><script>const output = document.getElementById(\"output\");const code = document.getElementById(\"code\");let cpt = 0;function addToOutput(s) {cpt += 1;output.value += 'In  ['+ cpt+ ']: ' + code.value + '\\n';output.value += 'Out [' + cpt+ ']: ' + s + '\\n\\n'; }output.value = 'Je me prépare...\\n'; async function main(){  await loadPyodide({ indexURL : 'https://cdn.jsdelivr.net/pyodide/v0.17.0/full/' });  output.value += 'Prêt!\\n';} let pyodideReadyPromise = main(); async function evaluatePython() {  await pyodideReadyPromise;  try { let output = await pyodide.runPythonAsync(code.value); addToOutput(output);  } catch(err) { addToOutput(err);}} </script>"
    return s


def define_env(env):
    "Hook function"
    
    # activate trace
    chatter = env.start_chatting("Simple module")# activate trace
    #voir https://mkdocs-macros-plugin.readthedocs.io/en/latest/troubleshooting/
    env.variables['compteur_exo'] = 0
    env.variables['compteur_enigme'] = 0
    env.variables['term_counter'] = 0
    env.variables['IDE_counter'] = 0   

    #@env.macro
    #def exercice(): #G Connan
    #   env.variables['compteur_exo'] += 1
    #   return f"tip \"Exercice { env.variables['compteur_exo']}\""

    @env.macro
    def enigme(): 
       env.variables['compteur_enigme'] += 1
       return f"question \"Énigme { env.variables['compteur_enigme']}\""

    @env.macro
    def console(): #G Connan
        return pyodide()
    

    @env.macro
    def console_perso(url_code_test, url_code_template, url_pyodid = f"../../javascripts/pyodid.js"):        
        code_test = f"""
        --8<---  "docs/""" + os.path.dirname(env.variables.page.url.rstrip('/')) + f"""/{url_code_test}"
        """    
        code_template =  f"""
        --8<---  "docs/""" + os.path.dirname(env.variables.page.url.rstrip('/')) + f"""/{url_code_template}"
        """              
        s = "<div>Code:</div><textarea id='code' class='txta' autofocus></textarea>"          
        s = s + "<button onclick='evaluatePythonPerso()'  class='execution'>Exécuter le code</button> <button class='execution' onclick='clearOutput()'>Nettoyer Console</button>"
        s = s + "<div>Évaluation du code :</div><textarea id='output' class='txta common'></textarea><br><br><button onclick='executeTestPerso(code_test)'  class='execution'>Exécuter les tests unitaires</button>  <button class='execution' onclick='clearSortieTest()'>Nettoyer tests</button><div>Évaluation des tests :</div><textarea id='sortie_test' style='width: 100%;' rows='6' disabled></textarea>" 
        s = s + f"<script src='{url_pyodid}'></script>"   
        s = s +  "<script>var code_template =`" + code_template + "`;" + "code_template = desindente(code_template);code.innerHTML = code_template </script>"
        s = s +  "<script>var code_test =`" + code_test + "`;" + "code_test = desindente(code_test);</script>"
        return s  

   

    @env.macro
    def basthon(exo: str, hauteur: int) -> str: #F Chambon
        "Renvoie du HTML pour embarquer un fichier `exo` dans Basthon"
        return f"""<iframe src="https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo}" width=100% height={hauteur}></iframe>
[Lien dans une autre page](https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo})
"""

    @env.macro
    def script(lang: str, nom: str) -> str: #F Chambon
        "Renvoie le script dans une balise bloc avec langage spécifié"
        return f"""```{lang}
--8<---  "docs/""" + os.path.dirname(env.variables.page.url.rstrip('/')) + f"""/{nom}"
```"""
    #voir https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#snippets

    @env.macro
    def py(nom: str) -> str: #F Chambon
        "macro python rapide"
        return script('python', "scripts/" + nom + ".py")

    @env.macro
    def html_fig(num: int) -> str: #F Chambon
        "Renvoie le code HTML de la figure n° `num`"
        return f'--8<-- "docs/' + os.path.dirname(env.variables.page.url.rstrip('/')) + f'/figures/fig_{num}.html"'


   

    @env.macro
    def table_a():  #F Chambon
        a = [1, 1, 2, 3]
        b = [1, 1, 3, 4]
        c = [1, 1, 2, 3]
        for n in range(4, 24):
            # On ajoute a[n], puis b[n], puis c[n]
            a.append(a[n-1] + a[n-2] + a[n-4] + 2*b[n-4] + c[n-4])
            b.append(a[n] + b[n-2])
            c.append(a[n] + c[n-4])

        def markdown(a, ni, nf):
            """Renvoie un joli tableau markdown des valeurs de
            la suite a_n pour n dans [ni, nf["""
            ans = "|$n$|"
            for n in range(ni, nf): ans += f"${n}$|"
            ans += "\n|:---:|"
            for n in range(ni, nf): ans += ":---:|"
            ans+= "\n|$a_n$|"
            for n in range(ni, nf): ans += f"${a[n]}$|"
            return ans + "\n\n"

        return markdown(a, 0, 24)

    #Macros terminal et REPL Vincent Bouillot
    @env.macro
    def terminal() -> str:
        """   
        Purpose : Create a Python Terminal.
        Methods : Two layers to avoid focusing on the Terminal. 1) Fake Terminal using CSS 2) A click hides the fake 
        terminal and triggers the actual Terminal.
        """        
        tc = env.variables['term_counter']
        env.variables['term_counter'] += 1
        return f"""<div onclick='start_term("id{tc}")' id="fake_id{tc}" class="terminal_f"><label class="terminal"><span>>>> </span></label></div><div id="id{tc}" class="hide"></div>"""

    def read_ext_file(nom_script : str) -> str:
        """
        Purpose : Read a Python file that is uploaded on the server.
        Methods : The content of the file is hidden in the webpage. Replacing \n by a string makes it possible
        to integrate the content in mkdocs admonitions.
        """
        short_path = f"""docs/{os.path.dirname(env.variables.page.url.rstrip('/'))}"""
        try: 
            f = open(f"""{short_path}/scripts/{nom_script}.py""")
            content = ''.join(f.readlines())
            f.close()
            content = content+ "\n"
            # Hack to integrate code lines in admonitions in mkdocs
            return content.replace('\n','backslash_newline')
        except :
            return
        
    def generate_content(nom_script : str) -> str:
        """
        Purpose : Return content and current number IDE {tc}.
        """
        tc = env.variables['IDE_counter']
        env.variables['IDE_counter'] += 1

        content = read_ext_file(nom_script)

        if content is not None :
            return content, tc
        else : return "", tc

    def create_upload_button(tc : str) -> str:
        """
        Purpose : Create upoad button for a IDE number {tc}.
        Methods : Use an HTML input to upload a file from user. The user clicks on the button to fire a JS event
        that triggers the hidden input.
        """
        return f"""<button class="emoji" onclick="document.getElementById('input_editor_{tc}').click()">⤴️</button>\
                <input type="file" id="input_editor_{tc}" name="file" enctype="multipart/form-data" class="hide"/>"""

    def create_unittest_button(tc: str, nom_script: str, mode: str) -> str:
        """
        Purpose : Generate the button for IDE {tc} to perform the unit tests if a valid test_script.py is present.
        Methods : Hide the content in a div that is called in the Javascript
        """
        stripped_nom_script = nom_script.split('/')[-1]
        relative_path = '/'.join(nom_script.split('/')[:-1])
        nom_script = f"{relative_path}/test_{stripped_nom_script}"
        content = read_ext_file(nom_script)
        if content is not None: 
            return f"""<span id="test_term_editor_{tc}" class="hide">{content}</span><button class="emoji_dark" onclick=\'executeTest("{tc}","{mode}")\'>🛂</button><span class="compteur">5/5</span>"""
        else: 
            return ''


    def blank_space() -> str:
        """ 
        Purpose : Return 5em blank spaces. Use to spread the buttons evenly
        """
        return f"""<span style="indent-text:5em"> </span>"""

    @env.macro
    def IDEv(nom_script : str ='') -> str:
        """
        Purpose : Easy macro to generate vertical IDE in Markdown mkdocs.
        Methods : Fire the IDE function with 'v' mode.
        """
        return IDE(nom_script, 'v')


    @env.macro
    def IDE(nom_script : str ='', mode : str = 'h') -> str:
        """
        Purpose : Create a IDE (Editor+Terminal) on a Mkdocs document. {nom_script}.py is loaded on the editor if present. 
        Methods : Two modes are available : vertical or horizontal. Buttons are added through functioncal calls.
        Last span hides the code content of the IDE if loaded.
        """
        content, tc = generate_content(nom_script)
        corr_content, tc = generate_content(f"""{'/'.join(nom_script.split('/')[:-1])}/corr_{nom_script.split('/')[-1]}""")
        div_edit = f'<div class="ide_classe">'
        if mode == 'v':
            div_edit += f'<div class="wrapper"><div class="interior_wrapper"><div id="editor_{tc}"></div></div><div id="term_editor_{tc}" class="term_editor"></div></div>'
        else:
            div_edit += f'<div class="wrapper_h"><div class="line" id="editor_{tc}"></div><div id="term_editor_{tc}" class="term_editor_h terminal_f_h"></div></div>'
        div_edit += f"""<button class="emoji" onclick='interpretACE("editor_{tc}","{mode}")'>▶️</button>"""
        div_edit += f"""{blank_space()}<button class="emoji" onclick=\'download_file("editor_{tc}","{nom_script}")\'>⤵️</button>{blank_space()}"""
        div_edit += create_upload_button(tc)
        div_edit += create_unittest_button(tc, nom_script, mode)
        div_edit += '</div>'

        div_edit += f"""<span id="content_editor_{tc}" class="hide">{content}</span>"""
        div_edit += f"""<span id="corr_content_editor_{tc}" class="hide">{corr_content}</span>"""
        return div_edit

    # rajout
    #env.variables['compteur_exo'] = 0
    @env.macro
    def exercice(var = True, prem = None):
        # si var == False, alors l'exercice est placé dans une superfence.
        if prem is not None : env.variables['compteur_exo'] = prem
        env.variables['compteur_exo'] += 1
        root = f"Exercice { env.variables['compteur_exo']}"
        return f"""exo \"{root}\"""" if var else '\"'+root+'\"'

    @env.macro
    def cours():
        return f'done "Cours"'

    @env.macro
    def ext():
        return f'ext "Pour aller plus loin"'

    @env.macro
    def tit(ch = "", text = ""):
        # Tasklist In Table
        checked = 'checked=""' if ch == 'x' else ''
        return f"""<ul class="task-list"><li class="task-list-item">\
            <label class="task-list-control"><input type="checkbox" {checked}>\
            <span class="task-list-indicator"></span>\
            </label>{text}</li></ul>"""


    env.variables['transversal']=["histoire","projet","typesconstruits","python"]
    env.variables['projet'] = {"icone":":fontawesome-solid-lightbulb:","style":"projet"}
    env.variables['typesconstruits'] = {"icone":":fontawesome-solid-cubes:","style":"typesconstruits"}
    env.variables['python'] = {"icone":":fontawesome-brands-python:","style":"python"}
    env.variables['themes']={
        "histoire":"Histoire de l'informatique",
        "projet":"Projet",
        "sd":"Structures de données",
        "db":"Bases de données",
        "os":"Architectures matérielles, systèmes d'exploitation et réseaux",
        "algorithmique":"Algorithmique",
        "python":"Langages et programmation"
    }
    env.variables['icones'] = {
        "histoire":':fontawesome-solid-university:{title="'+env.variables['themes']['histoire']+'"}',
        "projet":':fontawesome-solid-lightbulb:{title="'+env.variables['themes']['projet']+'"}',
        "sd":':fontawesome-solid-project-diagram:{title="'+env.variables['themes']['sd']+'"}',
        "db":':fontawesome-solid-database:{title="'+env.variables['themes']['db']+'"}',
        "os":':fontawesome-solid-microchip:{title="'+env.variables['themes']['os']+'"}',
        "algorithmique":':fontawesome-solid-cogs:{title="'+env.variables['themes']['algorithmique']+'"}',
        "python":':fontawesome-brands-python:{title="'+env.variables['themes']['python']+'"}'
    }
    env.variables['icones_exo']={
        "dur": ":fontawesome-solid-bomb:{title='Exercice difficile'}",
        "rappel": ":fontawesome-solid-history:{title='Retour sur des notions antérieures'}",
        "recherche": ":fontawesome-solid-search:{title='Exercice de recherche'}",
        "capacite": ":fontawesome-solid-puzzle-piece:{title='Exercice testant une capacité du chapitre'}",
        "python": ":fontawesome-brands-python:{title='Exercice en lien avec la programmation en Python'}",
        "bac": ":fontawesome-solid-graduation-cap:{title='Exercice extrait du Bac'}",
        "maths": ":fontawesome-solid-infinity:{title='Exercice en lien avec les mathématiques'}"
    }
    env.variables['icones_act']={
        "rappel": ":fontawesome-solid-history:{title='Retour sur des notions antérieures'}",
        "recherche": ":fontawesome-solid-search:{title='Activité de recherche'}",
        "oral": ":fontawesome-solid-comments:{title='Activité oral'}",
        "papier": ":fontawesome-solid-edit:{title='Activité à réaliser sur feuille'}",
        "vscode": ":material-microsoft-visual-studio-code:{title='Activité utilisant VS Code'}",
        "video": ":fontawesome-solid-film:{title='Activité utilisant un support vidéo'}",
        "notebook": ":fontawesome-solid-book:{title='Activité utilisant un jupyter notebook'}",
        "python": ":fontawesome-brands-python:{title='Activité en lien avec la programmation en Python'}"
    }
    env.variables['devant_exo']=':black_small_square:'
    env.variables['devant_act']=':black_small_square:'
    env.variables['num_exo']=1
    env.variables['num_act']=1

    env.variables['progression']={
        0 : ["python","Révisions",2,"revisions.md"],
        1 : ["python","Récursivité",2,"recursivite.md"],
        2 : ["db","Bases de données et SQL",2,"sql.md"],
        3 : ["os","Processus",1,"processus.md"],
        4 : ["algorithmique","Diviser pour régner",1,"diviser.md"],
        5 : ["python","Notions de programmation orienté objet",2,"poo.md"],
        6 : ["sd","Structures de données linéaires",2,"sl.md"],
        7 : ["os","Système sur puce",1,"puces.md"],
        8 : ["sd","Arbres",2,"arbres.md"],
        9 : ["db","Schéma relationnel d'une base de données",2,"sgbd.md"],
        10: ["algorithmique","Algorithmes sur les arbres",2,"algoarbre.md"],
        11: ["sd","Graphes",2,"graphes.md"],
        12: ["os","Protocoles de routage",2,"routage.md"],
        13: ["algorithmique","Recherche textuelle",2,"texte.md"],
        14: ["python","Calculabilité, décidabilité",2,"calculabilite.md"],
        15: ["os","Sécurisation des communications",2,"cryptage.md"]
    }

    env.variables['nchap']=0
    env.variables['nelements']=0
    
    # Titres des items travaillés sur l'année
    @env.macro
    def sec_titre(theme,titre):
            icone = env.variables.icones[theme]
            return f"### {icone} &nbsp; {titre}"
    
    @env.macro
    def icone(theme):
        return env.variables.icones[theme]
    
    @env.macro
    def titre_chapitre(numero,titre,theme):
        # Position de l'ancre pour repérage dans la page
        titre_bis = env.variables['progression'][numero][1]
        ligne=f"# {titre} "   # f"# <span class='numchapitre'>C{numero}</span> {titre_bis} "
        ligne+=f"<span style='float:right;'>{env.variables.icones[theme]}</span>"
        return ligne
    
    @env.macro
    def titre_chap(titre,theme):
        # Position de l'ancre pour repérage dans la page
        ligne=f"# {titre} "
        ligne+=f"<span style='float:right;'>{env.variables.icones[theme]}</span>"
        return ligne

    @env.macro
    def titre_activite(titre,licones,numero=1):
        if numero==0:
            env.variables['num_act']=1
        # modif me
        if numero==-1:
            env.variables['num_act']=0
        #
        ligne=f"### {env.variables['devant_act']}   Activité {env.variables['num_act']} "
        if titre!="":
            ligne+=f": *{titre}*"
        if licones!=[]:
            ligne+=f"<span style='float:right;'>"
            for icone in licones:
                ligne+=f"<span style='float:right;'>&thinsp; {env.variables['icones_act'][icone]}</span>"
            ligne+="</span>"
        env.variables['num_act']=env.variables['num_act']+1
        return ligne
    

    @env.macro
    def exo(titre,licones,numero=1):
        if numero==0:
            env.variables['num_exo']=1
        ligne=f"### {env.variables['devant_exo']}   Exercice {env.variables['num_exo']} "
        if titre!="":
            ligne+=f": *{titre}*"
        if licones!=[]:
            ligne+=f"<span style='float:right;'>"
            for icone in licones:
                ligne+=f"<span style='float:right;'>&thinsp; {env.variables['icones_exo'][icone]}</span>"
            ligne+="</span>"
        env.variables['num_exo']=env.variables['num_exo']+1
        return ligne
    
    @env.macro
    def liens(fichier,type=".pdf"):
        location="./pdf/"+fichier[0:2]+"/"
        return f"[:fontawesome-solid-download:]({location}{fichier}.pdf) [:fontawesome-regular-file:]({location}{fichier}.tex)"

    @env.macro
    def telecharger(description,fichier):
        liens =f"[{description} :material-download:](./{fichier})"
        liens+="{.md-button}"
        return "<span class='centre'>"+liens+"</span>"

    @env.macro
    def televerser(description,url):
        liens =f"[{description} :material-download:]({url})"
        liens+="{.md-button}"
        return "<span class='centre'>"+liens+"</span>"

    @env.macro
    def grim(fichier):
        ccours = '''
Vous pouvez télécharger une copie au format pdf du diaporama de synthèse de cours présenté en classe :

<span class='centre'>[Diaporama de cours :fontawesome-regular-file-pdf:](./pdf/'''+fichier+'''){.md-button}</span>
!!! warning "Attention"
    Ce diaporama ne vous donne que quelques points de repères lors de vos révisions. Il devrait être complété par la relecture attentive de vos **propres** notes de cours et par une révision approfondie des exercices.'''
        return ccours

    @env.macro
    def aff_cours(num):
        fichier=f'C{num}/C{num}-cours.pdf'
        return grim(fichier)

    @env.macro
    def sc(chaine):
        return f'<span style="font-variant:small-caps;">{chaine}</span>'

    @env.macro
    def chapitre(num,theme,titre,duree,lien):
        icone = env.variables["icones"][theme]
        return f"|{icone}|[C{num}- {titre}]({lien}) | {duree}\n"

    @env.macro
    def affiche_progression():
        ret='''
| |Titre        | Durée |
|-|-------------|-------|
        '''
        for k in env.variables.progression:
           ret+=chapitre(k,env.variables['progression'][k][0],env.variables['progression'][k][1],env.variables['progression'][k][2],env.variables['progression'][k][3])
        return ret
    
    @env.macro
    def genere_nav():
        ret='''```\n'''
        for k in env.variables.progression:
            da = env.variables['progression'][k]
            ret+=f'  - "C{k}-{da[1]}" : {da[3]}\n'
        return ret+'```\n'
    
    @env.macro
    def ok():
        return ":fontawesome-solid-check:{.vert title='Compatible'}"
    
    @env.macro
    def nok():
        return ":fontawesome-solid-times:{.rouge title='Non compatible'}"
    
    @env.macro
    def ep(annee):
        aff="\n"
        aff+= "|Numéro | Lien de téléchargement| Thème exercice 1 | Thème exercice 2  | Code fourni |\n"
        aff+= "|-------|-----------------------|------------------|-------------------|-------------|\n"
        FNAME = f"./docs/officiels/Annales/EP/{annee}/l{annee}.txt"
        with open(FNAME,"r",encoding="utf-8") as f:
            nums=1
            for s in f:
                lf=s.split(",",2)
                aff+=f"|{nums}|[Sujet N°{nums}](./officiels/Annales/EP/{annee}/{lf[0]}/{lf[0]}.pdf) | {lf[1]} | {lf[2][:-1]} | [:material-download: Code](./officiels/Annales/EP/{annee}/{lf[0]}/{lf[0]}.py) \n"
                nums+=1
        return aff
    