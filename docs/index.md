# Spécialité NSI en Terminale

## Introduction

Ce site web vous propose un cours interactif permettant de programmer en Python sans avoir besoin d'installer un éditeur (Merci à V. Bouillot). 

<!-- Utiliser le navigateur **Firefox**, **Chrome** ou **Chromium** pour bénéficier de la meilleure expérience utilisateur.-->

La programmation s'apprend avant tout en lisant, en écrivant, en répétant et en s'entrainant.

Faites les exercices **corrigés** et préparez les questions que nous traiterons en cours.


## Fonctionnement des exercices

Trois grands types d'exercices sont proposés.

!!! example "Papier/Crayon"

    Pas de problème. Prenez une feuille et un crayon. Ne trichez pas en regardant la correction trop vite.

!!! example "Prédire/comprendre"

    Comme ci-dessous, vous disposez d'un programme dans un éditeur. Vous devez comprendre le programme et prédire ce qui va se passer. Vous pouvez tester en appuyant sur la flèche pointant à droite.

    {{IDE('intro')}}

!!! example "Programmer"

    Vous devez compléter ou écrire un programme dans un éditeur. 

    - Vous pouvez tester en appuyant sur la flèche pointant à droite ▶️. 
    - Vous pouvez tenter de valider votre programme pour savoir si celui-ci est correct en cliquant sur le gendarme 🛂. Votre programme est alors soumis à de nombreux tests. 
    - Au bout de 5 validations ratées, la solution apparait.

    {{IDE('exo2')}}

## Terminal, console et éditeur de code

Un terminal est une invite de commandes permettant à l'homme et à la machine de communiquer de manière interactive.

Pour programmer, il existe deux grands types d'outils :

- la console (aussi appelé interpréteur) permet de réaliser des calculs, des tests rapides ainsi que des programmes courts. On ne sauvegarde pas son travail ;
- l'éditeur de code permet d'écrire un programme complexe dans un langage donné et de sauvegarder son travail. Il est souvent combiné à un interpréteur afin d'exécuter le programme et de le rendre compréhensible par l'ordinateur.

!!! example "Exemple"

    === "Une console"

        - [ ] Calculez la somme de 134 et de 5677 dans la console. Pour valider, appuyez sur ++enter++. 
        - [ ] Faites également leur multiplication à l'aide de l'opérateur `*`.

        {{terminal()}}

    === "Un éditeur de code"

        Lancez le script à l'aide de la flèche.

        {{IDEv('python1/ex1')}}
    


## FAQ

!!! help "Rien ne s'enregistre et lorsque je recharge la page internet, tout s'efface !"

    C'est normal. Il n'y a pas de cookie ou de sessions, vos données ne sont donc pas enregistrées mais vous pouvez télécharger vos programmes lorsqu'ils sont importants.
