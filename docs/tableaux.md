
{% set titre = "Les tableaux/listes de Python" %}
{% set theme = "python" %}

{{ titre_chap(titre,theme)}}

En première, nous avons apris à utiliser les tableaux, une structure de donnée séquentielle disponible directement pour le programmeur avec une syntaxe adaptée.

Commençons par faire le point sur cette structure.
 
## Quelques rappels sur les tableaux/listes de Python

En python, on peut définir un tableau grâce à la syntaxe suivante:

```py3
>>> tableau = [1, 1, 2, 3, 5, 8, 13, 21]
```
On peut à la fois accéder et modifier un élément de rang donné:
```py3
>>> tableau[6]
13

>>> tableau[0] = -1
>>> tableau
[-1, 1, 2, 3, 5, 8, 13, 21]
```
Enfin, il est possible de connaître la longueur d'un tableau:
```py3
>>> len(tableau)
8
```
Ce sont là les seules opérations élémentaires que doit satisfaire une structure de donnée appelée tableau. Précisons un peu cette notion:

=== "L'interface d'un tableau"
    ??? example "L'interface d'un tableau"

        D'un point de vue abstrait, un **tableau** est une structure de donnée répondant aux critères suivants:
        !!! aide  
            Voir [cette page](https://fklag.gitlab.io/tnsi_structures_donnees/python4/){target=_blank} du cours.   
            L'**interface d'une structure de donnée** est l'ensemble des opérations de base permettant d'interagir avec celle-ci.  
            Le programmeur peut (et doit exclusivement) utiliser les opérations définies par l'interface pour interagir avec une structure de donnée.

        - Il s'agit d'un ensemble de **données homogènes** (c'est-à-dire du même type) rangée de manière séquentielle;
        - La **taille** du tableau, encore appelée longueur, est fixe et connue au moment de la création du tableau;
        - On peut lire toutes les valeurs d'un tableau en connaissant son **rang**.  
        Les rangs sont des entiers compris entre $0$ et $N-1$ , où $N$ est la longueur du tableau;
        - On peut modifier n'importe quelle valeur du tableau de rang donné;
        - On peut connaître la longueur du tableau;
        - Un tableau vide est un tableau de longueur $0$.

        
    On constate que les tableaux tels qu'implémentés en `Python` répondent bien à ces critères.  
    Mais on a aussi vu en Première que les tableaux du langage `Python` peuvent faire bien plus.  
    Notamment (et la liste est loin d'être exhaustive):  

    - On peut ajouter des éléments à la fin du tableau (changeant par la même la taille du tableau dynamiquement en cours de programme):
      ```py3
      >>> tableau.append(34)
      >>> tableau
      [-1, 1, 2, 3, 5, 8, 13, 21, 34]
      ```

    - À l'inverse, on peut retirer le dernier élément du tableau (s'il existe):  
      ```py3
      >>> tableau.pop()
      34
      >>> tableau
      [-1, 1, 2, 3, 5, 8, 13, 21]
      ```
    
    - Mieux encore: il est possible d'insérer (ou de retirer) des éléments de rang quelconque du tableau:  
      ```py3
      >>> tableau.insert(2, 7)
      >>> tableau
      [-1, 1, 7, 2, 3, 5, 8, 13, 21, 34]
      >>> 
      ```
    
    !!! attention
        Ces fonctions ne font pas partie de l'interface d'un tableau, mais plutôt d'une liste.  
        Les tableaux du langage `Python` sont en fait un type hybride ayant à la fois les caractéristiques d'un tableau mais aussi d'une liste. D'ailleurs, en `Python`, les tableaux s'appellent des listes.

        Dans ce cours, nous parlerons de **tableau/liste** pour désigner ce type hybride du langage `Python`. Nous nous astreindrons cependant à toujours bien distinguer les cas d'utilisation en tant que tableau pur, ou bien en tant que liste pure.

=== "L'interface d'une liste"
    ??? example "L'interface d'une liste"

        !!! aide  
            Voir [cette page](https://fklag.gitlab.io/tnsi_structures_donnees/python5/){target=_blank} du cours. 

        Une **liste** est un type de valeur satisfaisant aux critères suivants:

        - Il s'agit d'un ensemble de **données homogènes** (c'est-à-dire du même type) rangée de manière séquentielle;  
        - La **taille** d'une liste , encore appelée longueur, est variable: elle peut changer au cours de la durée de vie de la liste;  
        - Il est possible d'insérer et de supprimer des éléments en tête de liste;  
        - Il est possible d'insérer et de supprimer des éléments en queue de liste;  
        - Il est possible d'insérer et de supprimer des éléments à n'importe quelle position de la liste;  
        - Il est possible de parcourir tous les éléments de la liste, du premier jusqu'au dernier;  
        - Une liste vide ne contient aucun élément, sa longueur est $0$.

    Les **tableaux/listes** de `Python` répondent à tous ces critères (et bien plus encore). Cependant, nous allons voir que certaines fonctions de ces interfaces (à la fois pour les tableaux mais aussi les listes) ont un coût caché à l'exécution (qui est imperceptible pour des tableaux/listes ne contenant que peu de données, mais devient non négligeable lorsque l'on travaille sur des données pouvant atteindre plusieurs mégaoctets, voire gigaoctets).

=== "Coûts cachés de l'utilisation des tableaux/listes"
    !!! aide "Complexité"
        Le **coût** d'un algorithme, encore appelé [**complexité**](https://lgt-beaussier-83512-moodle.atrium-sud.fr/course/view.php?id=839){target=_blank}, est la relation entre la taille des données en entrée et le nombre d'étapes nécessaires à son exécution.   
        
    Jusqu'à présent (depuis le début de la Première), nous avons rencontré 3 types de coûts:

    - Un algorithme à **coût constant** demande, comme son nom l'indique, un nombre constant d'opérations, indépendemment de la taille des données en entrée.

    ??? example "Des exemples d'algorithmes à coût constant pour les tableaux/listes du langage `Python`"
        - Lire ou changer une valeur de rang $i$ dans un tableau;  
          
            Il suffit à l'interpréteur `Python` de calculer la position en mémoire de la valeur recherchée, puis de la lire. Le coût ne dépend pas de la taille du tableau;

        - Connaître la longueur d'un tableau;

            En `Python` la taille du tableau est stockée conjointement aux valeurs qu'il contient, il suffit donc de lire le contenu d'une variable (cachée pour l'utilisateur);

        - Savoir si un tableau est vide ou non;

            Il suffit de tester si la longueur est nulle, le coût est donc constant.

    - Un algorithme à un **coût linéaire** lorsque le nombre d'opérations nécessaires à son exécution est proportionnel à la taille des données en entrée.  
      La constante de proportionnalité n'est en général pas précisée (et elle peut être relativement grande selon les algorithmes), mais ce qui est important de retenir est que, par exemple, si on double la taille des données, on double le temps d'exécution: c'est ce que signifie proportionnalité.

    ??? example "Des exemples d'algorithmes à coût linéaire pour les tableaux/listes du langage `Python`"
        - Ajouter un élément à une position quelconque du tableau: il y a un double coût caché, car il peut arriver que `Python` doive réserver un nouvel emplacement mémoire (plus grand) pour ajouter un élément, nécessitant alors de recopier l'intégralité du tableau (d'où le coût linéaire), avec éventuellement un décalage.

            Examinez ce code sur l'excellent site PythonTutor qui met artificiellement en évidence les opérations nécessaires pour «ajouter» un élément en tête d'un tableau. Tout ceci est bien évidemment invisible lorsque l'on utilise `#!python tableau.insert(0, valeur)`, mais le coût est néanmoins là. Le fait qu'il faille recopier $N-1$ éléments pour une liste de taille $N$ ne change rien au fait que le coût est bien linéaire.

            <iframe width="1200" height="450" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20ajoute_element%28tete,%20tableau%29%3A%0A%20%20%20%20nouveau_tableau%20%3D%20%5B%22%22%5D*%28len%28tableau%29%20%2B%201%29%0A%20%20%20%20for%20i%20in%20range%28len%28tableau%29%29%3A%0A%20%20%20%20%20%20%20%20nouveau_tableau%5Bi%2B1%5D%20%3D%20tableau%5Bi%5D%0A%20%20%20%20nouveau_tableau%5B0%5D%20%3D%20tete%0A%20%20%20%20return%20nouveau_tableau%0A%20%20%20%20%0Atableau%20%3D%20list%28%22Beaussier%22%29%0Atableau%20%3D%20ajoute_element%28%22%23%22,%20tableau%29&cumulative=false&heapPrimitives=false&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

        - Retirer un élément à une position quelconque du tableau: en général on n'a pas besoin de réserver un nouvel emplacement en mémoire, mais il faut néanmoins déplacer tous les éléments à droite de l'élément supprimé pour boucher le trou: dans le pire des cas, on déplace $N-1$ éléments, où $N$ est la taille du tableau. On a donc bien un coût linéaire.

            Voici encore une mise en évidence de ce qu'il est nécessaire de faire, toujours avec PythonTutor. On observe bien le coût proportionnel à la longueur de la liste.

            <iframe width="1200" height="650" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20efface_element%28tableau,%20rang%29%3A%0A%20%20%20%20assert%20rang%20%3E%3D%200%0A%20%20%20%20assert%20rang%20%3C%20len%28tableau%29%0A%20%20%20%20%0A%20%20%20%20nouveau_tableau%20%3D%20%5B%22%22%5D*%28len%28tableau%29%20-%201%29%0A%20%20%20%20%23%20On%20recopie%20les%20%C3%A9l%C3%A9ments%20*avant*%20le%20rang%20%C3%A0%20effacer%0A%20%20%20%20for%20i%20in%20range%28rang%29%3A%0A%20%20%20%20%20%20%20%20nouveau_tableau%5Bi%5D%20%3D%20tableau%5Bi%5D%0A%20%20%20%20%20%20%20%20%0A%20%20%20%20%23%20On%20recopie%20les%20%C3%A9l%C3%A9ments%20*apr%C3%A8s*%20le%20rang%20%C3%A0%20effacer%0A%20%20%20%20for%20i%20in%20range%28rang%20%2B%201,%20len%28tableau%29%29%3A%0A%20%20%20%20%20%20%20%20nouveau_tableau%5Bi-1%5D%20%3D%20tableau%5Bi%5D%0A%20%20%20%20return%20nouveau_tableau%0A%20%20%20%20%0Atableau%20%3D%20list%28%22NUM%C3%89RIQUE%22%29%0Atableau%20%3D%20efface_element%28tableau,%204%29&cumulative=false&heapPrimitives=false&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    - Un algorithme à un **coût quadratique** lorsque le nombre d'opérations nécessaires à son exécution est proportionnel au carré de la taille des données.  
      Il faut retenir que lorsque l'on double la taille des données pour un tel algorithme, le temps d'exécution est multiplié par $2^2=4$ ; de même, si on multiplie la taille des données par $3$, le temps d'exécution est multiplié par $9$.

    ??? example "Les seuls algorithmes quadratiques rencontrés pour l'instant sont les algorithmes de tri par insertion et tri par sélection étudiés en première"
        Aucun des algorithmes des interfaces des tableaux ou des listes ne nécessitent un coût quadratique.

    !!! attention "Attention, le coût d'un des algorithmes définissant l'interface d'un tableau ou d'une liste dépend de l'implémentation particulière de cette structure de donnée"
        Il existe presque tout le temps de multiples façons de définir une structure de donnée afin qu'elle satisfasse à une interface donnée, et les coûts peuvent être très variables d'une opération à l'autre.