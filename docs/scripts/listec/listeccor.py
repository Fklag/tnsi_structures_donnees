class Maillon:
    def __init__(self):
        self.val = None
        self.suiv = None # Pas de maillon suivant

    def __str__(self):
        if  self.suiv is not None:
            return str(self.val) + "-" + str(self.suiv.val)
        else:
            return str(self.val) + "-" + str(self.suiv)

class ListeC:
    def __init__(self):
       self.tete = None # Liste vide


L1 = ListeC()

L = ListeC()
M1, M2 = Maillon(), Maillon()
M1.val = 1
M1.suiv = M2
M2.val = 2
L.tete = M1

def est_vide(L):
   return L.tete is None

def taille(L):
    """ Renvoie le nombre de Maillons de la liste L """
    m = L.tete
    l = 0
    while m is not None:
        l += 1
        m = m.suiv
    return l

def get_dernier_maillon(L):
    """ Renvoie le dernier Maillon de la liste L """
    m = L.tete
    M = m
    while m is not None:
        M = m
        m = m.suiv
    return M

def get_maillon_indice(L, i):
   """ Renvoie Maillon d'indice i dans la liste L """
   j = 0
   m = L.tete
   while j < i:
      j += 1
      m = m.suiv
   return m

def ajouter_debut(L, nM):
   """ Ajoute le maillon nM en tête de la liste L """
   nM.suiv = L.tete
   L.tete = nM

def ajouter_fin(L, nM):
   """ Ajoute le maillon nM en queue de la liste L """
   m = L.tete
   while m.suiv is not None:
      m = m.suiv
   m.suiv = nM

def ajouter_apres(L, M, nM):
   """ Ajoute le maillon M après celui d'indice i dans la liste L """
   nM.suiv = M.suiv
   M.suiv = nM

def supprimer_debut(L):
   """ Supprime le 1er maillon de la liste L et le renvoie """
   t = L.tete
   L.tete = L.suiv
   return t

def supprimer_fin(L):
   """ Supprime le dernier maillon de la liste L et le renvoie """
   m = L.tete
   while m.suiv.suiv is not None:
      m = m.suiv
   m.suiv = None
   return m

def supprimer_apres(L, M):
   """ Supprime le maillon de la liste L situé après le maillon M et le renvoie
   """
   s = M.suiv
   M.suiv = M.suiv.suiv
   return s


