L, l = ListeC(), ListeC()
M1, M2, m1, m2, m3 = Maillon(), Maillon(), Maillon(), Maillon(), Maillon()
M1.val, M1.suiv = 1, M2
M2.val, M2.suiv = 2, None
L.tete = M1
m1.val, m1.suiv = 1, m2
m2.val, m2.suiv = 2, m3
m3.val, m3.suiv = 3, None
l.tete =  m1

b1 = ['taille(ListeC()) == 0', 'taille(L) == 2', 'taille(l) == 3']
b2 = ['get_dernier_maillon(ListeC()) == None', 'get_dernier_maillon(L) == M2', 'get_dernier_maillon(l) == m3']

benchmark = (b1, b2)
