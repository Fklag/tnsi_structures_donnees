class Maillon:
    def __init__(self, valeur, suivant=None):
        self.valeur = valeur
        self.suivant = suivant



class Pile:
    def __init__(self):
        self.taille = 0 # nombre d'assiettes dans la pile
        self.sommet = None


    def empiler(self, valeur):
        self.sommet = Maillon(valeur, self.sommet)
        self.taille += 1

    def depiler(self):
        if self.taille > 0:
            valeur = self.sommet.valeur
            self.sommet = self.sommet.suivant
            self.taille -= 1
            return valeur

    def estVide(self):
        return self.taille == 0


    def lireSommet(self):
        return self.sommet.valeur


    def __str__(self):
        ch = "\nEtat de la pile:\n"
        sommet = self.sommet
        while sommet != None:
            ch += "|\t" + str(sommet.valeur) + "\t|" + "\n"
            sommet = sommet.suivant

        return ch



p = Pile()
p.empiler(9)
p.empiler(2)
p.empiler(5)

print(p)
print(p.lireSommet())
print(p.taille)


p.depiler()
p.depiler()
p.empiler(8)

print(p)
print(p.lireSommet())
print(p.taille)