class Maillon:
    def __init__(self):
        self.val = None
        self.suiv = None # Pas de maillon suivant

    def __str__(self):
        if  self.suiv is not None:
            return str(self.val) + "-" + str(self.suiv.val)
        else:
            return str(self.val) + "-" + str(self.suiv)

class ListeC:
    def __init__(self):
       self.tete = None # Liste vide

def est_vide(L):
   return L.tete is None