class Maillon():
    def __init__(self, data = None):
        self.data = data
        self.suiv = None

    def __repr__(self):
        return self.data.__repr__()

class File:
    def __init__(self):
        self.queue = None
        self.tete = None


    def enfiler(self, valeur):
        M = Maillon(valeur)
        if self.estVide():
            self.tete = M
        else :
            self.queue.suiv = M
        self.queue = M

    def defiler(self):
        if not self.estVide():
            M = self.tete
            self.tete = M.suiv
            return M.data
        raise ValueError("File vide")

    def estVide(self):
        return self.queue is None


    def lireAvant(self):
        return self.tete.data

    def taille(self):
        M = self.tete
        l = 0
        while M is not None:
            l += 1
            M = M.suiv
        return l

    def __str__(self):
        ch = "|"
        maillon = self.tete
        while maillon != None:
            ch = "|\t" + str(maillon.data) + "\t" + ch
            maillon = maillon.suiv
        return "\nEtat de la file:\n" + ch

    # def __repr__(self):
    #     m = self.tete
    #     l = []
    #     while m is not None:
    #         l.append(m.__repr__())
    #         m = m.suiv
    #     return " ".join(l)


q = File()
q.enfiler(9)
q.enfiler(2)
q.enfiler(5)

print(q)

q.defiler()
q.enfiler(7)

print("La file est-elle vide: ", q.estVide())

print(q)
print("Longueur de la file:", q.taille())