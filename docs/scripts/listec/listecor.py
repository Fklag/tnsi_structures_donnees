def vide():
    return None

def cons(x,L):
    return(x,L)

def ajouteEnTete(x,L):
    return cons(x,L)

def supprEnTete(L):
    return L[0],L[1]

def estVide(L):
    return L is None

def compte(L):
    if estVide(L):
        return 0
    return 1 + compte(L[1])

def tete(L):
    return L[0] if L else None

def queue(L):
    return L[1] if L else ()

nil = vide()
print(estVide(nil))
L = cons(5, cons(4, cons(3, cons(2, cons(1, cons(0,nil))))))
print(tete(L))
print(queue(L))
print(estVide(L))
print(compte(L))
L = ajouteEnTete(6,L)
print(compte(L))
x,L=supprEnTete(L)
print(x)
print(compte(L))
x,L=supprEnTete(L)
print(x)
print(compte(L))
print(tete(L))
print(queue(L))